(defproject com.gitlab.druidgreeneyes/locario "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-json "0.5.0"]
                 [ring/ring-core "1.6.3"]
                 [compojure "1.6.1"]
                 [http-kit "2.3.0"]
                 [org.clojure/data.json "0.2.6"]
                 [environ "1.1.0"]
                 [org.xerial/sqlite-jdbc "3.23.1"]
                 [toucan "1.14.0"]
                 [funcool/cats "2.2.0"]]
  :main ^:skip-aot com.gitlab.druidgreeneyes.locario.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :plugins [[lein-ring "0.12.5"]
            [lein-auto "0.1.3"]
            [lein-environ "1.1.0"]
            [jonase/eastwood "0.3.5"]]
  :ring {:handler com.gitlab.druidgreeneyes.locario.core/app
         :init com.gitlab.druidgreeneyes.locario.core/init!})

(ns com.gitlab.druidgreeneyes.locario.config
  (:require [environ.core :as e]))

(defn env
  ([k] (-> e/env (k) (or (throw (IllegalStateException. (str k " must be defined!"))))))
  ([k default] (-> e/env (k default))))

(def port
  (-> (env :locario-port "3000")
      (Integer/parseInt)))
(def base-path (-> (env :locario-base-path ".") (str "/")))
(def sqlite-file  (env :locario-sqlite-file "locario.db"))

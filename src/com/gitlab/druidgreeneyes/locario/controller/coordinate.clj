(ns com.gitlab.druidgreeneyes.locario.controller.coordinate
  (:require [com.gitlab.druidgreeneyes.locario.model :as m]
            [com.gitlab.druidgreeneyes.locario.util :as u]
            [com.gitlab.druidgreeneyes.locario.persistence.coordinate :as db]
            [com.gitlab.druidgreeneyes.locario.controller.response :as r]
            [compojure.core :as c]))

(defn post [req]
  (-> (:body req)
      ;; (update :id (fn [id] (or id (u/generate-id))))
      ;; (m/map->CoordinateInstance)
      (db/save!)
      (r/handle-failure)))

(defn get-by-id [req]
  (-> (get-in req [:params :id])
      (db/get-by-id)
      (r/handle-failure)))

(c/defroutes routes
  (c/GET "/coordinate/:id" [id] #'get-by-id)
  (c/POST "/coordinate" [] #'post))

(defn usage []
  "\ncoordinates:
  GET /coordinate/:id
    get coordinate by coordinate id
  POST /coordinate
    add a new coordinate entry
    sample:
      {
        \"id\": \"0eut94-fi3rjf-10r9un-asdiuy\",
        \"latitude\": 56.109487,
        \"longitude\": 34.56104
      }")

(ns com.gitlab.druidgreeneyes.locario.controller.response
  (:require [cats.monad.either :as e]
            [cats.core :as cats]
            [clojure.pprint :as pp]))

(defn response [body & {:keys [status headers]
                        :or {status 200 headers nil}}]
  {:status status
   :body body
   :headers headers})

(defn handle-failure [try & {:keys [success-status success-headers
                                    failure-status failure-headers
                                    success-mapper failure-mapper]
                             :or {success-status 200
                                  failure-status 500
                                  success-headers nil
                                  failure-headers nil
                                  success-mapper #'identity
                                  failure-mapper #'str}}]
  (-> try
      (e/branch
       (fn [f]
         (pp/pprint f)
         (-> (failure-mapper f)
             (response :status failure-status
                       :headers failure-headers)))
       (fn [s] (-> (success-mapper s)
                   (response :status success-status
                             :headers success-headers))))))

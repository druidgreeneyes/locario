(ns com.gitlab.druidgreeneyes.locario.controller.ping
  (:require [com.gitlab.druidgreeneyes.locario.model :as m]
            [com.gitlab.druidgreeneyes.locario.util :as u]
            [com.gitlab.druidgreeneyes.locario.persistence.ping :as db]
            [com.gitlab.druidgreeneyes.locario.controller.response :as r]
            [compojure.core :as c]))

(defn post [req]
  (-> (:body req)
      (db/save!)
      (r/handle-failure)))

(defn get-by-timestamp [req]
  (-> (get-in req [:params :timestamp])
      (db/get-by-timestamp)
      (r/handle-failure)))

(defn get-by-user [req]
  (-> (get-in req [:params :id])
      (db/get-by-user-id)
      (r/handle-failure)))

(defn get-by-coordinate [req]
  (-> (get-in req [:params :id])
      (db/get-by-coordinate-id)
      (r/handle-failure)))

(c/defroutes routes
  (c/GET "/ping/:timestamp" [timestamp] #'get-by-timestamp)
  (c/GET "/ping/by-user/:id" [id] #'get-by-user)
  (c/GET "/ping/by-coordinate/:id" [id] #'get-by-coordinate)
  (c/POST "/ping" [] #'post))

(defn usage []
  "\npings:
  GET /ping/:timestamp
    get ping identified by timestamp
  GET /ping/by-user/:id
    get all pings associated with user id
  GET /ping/by-coordinate/:id
    get all pings associated with coordinate id
  POST /ping
    add new ping
    sample:
      {
        \"timestamp\": 109874523097,
        \"user\": {
          \"user_id\": \"some-user\"
        },
        \"coordinate\": {
          \"latitude\": 56.109487,
          \"longitude\": 34.56104
        }
      }")

(ns com.gitlab.druidgreeneyes.locario.controller.user
  (:require [com.gitlab.druidgreeneyes.locario.model :as m]
            [com.gitlab.druidgreeneyes.locario.util :as u]
            [com.gitlab.druidgreeneyes.locario.persistence.user :as db]
            [com.gitlab.druidgreeneyes.locario.controller.response :as r]
            [compojure.core :as c]))

(defn post [req]
  (-> (:body req)
      ;; (update :user_id (fn [id] (or id (u/generate-id))))
      ;; (m/map->UserInstance)
      (db/save!)
      (r/handle-failure)))

(defn get-by-id [req]
  (-> (get-in req [:params :id])
      (db/get-by-id)
      (r/handle-failure)))

(c/defroutes routes
  (c/GET "/user/:id" [id] #'get-by-id)
  (c/POST "/user" [] #'post))

(defn usage []
  "\nusers:
  GET /user/:id
    get user by user id
  POST /user
    add a new user
    sample:
      {
        \"id\": \"139g1a-09ruhf-lk13bt-0u3rgw\"
      }")

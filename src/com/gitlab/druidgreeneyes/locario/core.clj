(ns com.gitlab.druidgreeneyes.locario.core
  (:gen-class)
  (:require [org.httpkit.server :as server]
            [compojure.core :as core]
            [compojure.route :as route]
            [ring.middleware.defaults :as d]
            [ring.middleware.json :as j]
            [toucan.db :as toucan]
            [toucan.models :as models]
            [com.gitlab.druidgreeneyes.locario.persistence.db :as db]
            [com.gitlab.druidgreeneyes.locario.config :as config]
            [com.gitlab.druidgreeneyes.locario.controller.response :as r]
            [com.gitlab.druidgreeneyes.locario.controller.coordinate :as coordinate]
            [com.gitlab.druidgreeneyes.locario.controller.user :as user]
            [com.gitlab.druidgreeneyes.locario.controller.ping :as ping]))

(defn show-usage [req]
  (r/response
   (str
    "Locario Usage:\n"
    (ping/usage)
    "\n"
    (coordinate/usage)
    "\n"
    (user/usage))))

(def my-site-defaults
  (assoc-in d/site-defaults [:security :anti-forgery] false))

(c/defroutes routes
  coordinate/routes
  user/routes
  ping/routes
  (c/GET "/" [] #'show-usage)
  (route/not-found "Error, page not found!"))

(def app (-> #'routes
             (d/wrap-defaults my-site-defaults)
             (j/wrap-json-response)
             (j/wrap-json-body {:keywords? true})))

(defn init! []
  (toucan/set-default-db-connection! db/db-spec)
  (models/set-root-namespace! 'com.gitlab.druidgreeneyes.locario.model))

(defn -main [& args]
  (server/run-server app {:port config/port})
  (println (str "Locario server started at port " config/port)))

(ns com.gitlab.druidgreeneyes.locario.model
  (:require [toucan.models :as m]
            [toucan.hydrate :as h]
            [clojure.pprint :as pp]
            [com.gitlab.druidgreeneyes.locario.util :as u]))

(m/defmodel Coordinate :coordinates
  m/IModel
  (hydration-keys [_]
                  [:coordinate])
  (pre-insert [coord]
              (println "Coordinate: Start Insert!")
              (pp/pprint coord)
              (pp/pprint (type coord))
              coord)
  (post-insert [coord]
               (println "Coordinate: Insert Complete!")
               (pp/pprint coord)
               (pp/pprint (type coord))
               coord))

(m/defmodel User :users
  m/IModel
  (hydration-keys [_]
                  [:user])
  (pre-insert [user]
              (let [i-user (update user :user_id #(or % (u/generate-id)))]
                (println "User: Start Insert!")
                (pp/pprint i-user)
                (pp/pprint (type i-user))
                i-user))
  (post-insert [user]
               (println "User: Insert Complete!")
               (pp/pprint user)
               (pp/pprint (type user))
               user))

(m/defmodel Ping :pings
  m/IModel
  (pre-insert [ping]
              (println "Ping: Start Insert!")
              (pp/pprint ping)
              (pp/pprint (type ping))
              ping)
  (post-insert [ping]
               (let [r-ping (h/hydrate ping :user :coordinate)]
                 (println "Ping: Insert Complete!")
                 (pp/pprint r-ping)
                 (pp/pprint (type r-ping))
                 r-ping)))

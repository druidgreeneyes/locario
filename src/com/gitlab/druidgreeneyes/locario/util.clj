(ns com.gitlab.druidgreeneyes.locario.util
  (:require [cats.monad.either :as e]
            [cats.core :as cats]
            [clojure.pprint :as pp]))

(defn generate-id []
  (.toString
   (java.util.UUID/randomUUID)))

(defn pprint-and-return [thing]
  (pp/pprint thing)
  thing)

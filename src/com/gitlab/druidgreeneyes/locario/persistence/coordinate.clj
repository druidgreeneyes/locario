(ns com.gitlab.druidgreeneyes.locario.persistence.coordinate
  (:require [toucan.db :as db]
            [com.gitlab.druidgreeneyes.locario.model :as m]
            [cats.monad.either :as e]
            [clojure.pprint :as pp]))

(defn save! [coordinate]
  (pp/pprint coordinate)
  (e/try-either
   (let [i-coord (db/select-one m/Coordinate
                                :latitude (:latitude coordinate)
                                :longitude (:longitude coordinate))]
     (if-not i-coord
       (let [res (db/insert! m/Coordinate coordinate)]
         (pp/pprint res)
         res)
       (do
         (println "No Insert necessary; coordinate already exists!")
         (pp/pprint i-coord)
         i-coord)))))

(defn get-by-id [id]
  (e/try-either
   (db/select-one m/Coordinate :id id)))

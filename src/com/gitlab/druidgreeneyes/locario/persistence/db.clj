(ns com.gitlab.druidgreeneyes.locario.persistence.db
  (:require [com.gitlab.druidgreeneyes.locario.config :as config]))

(def db-spec
  {:dbtype "sqlite"
   :dbname (str config/base-path config/sqlite-file)})

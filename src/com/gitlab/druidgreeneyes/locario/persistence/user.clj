(ns com.gitlab.druidgreeneyes.locario.persistence.user
  (:require [toucan.db :as db]
            [cats.monad.either :as e]
            [com.gitlab.druidgreeneyes.locario.model :as m]
            [clojure.pprint :as pp]
            [com.gitlab.druidgreeneyes.locario.util :as u]))

(defn save! [user]
  (pp/pprint user)
  (e/try-either
   (let [i-user (db/select-one m/User :user_id (:user_id user))]
     (if-not i-user
       (let [res (db/insert! m/User user)]
         (pp/pprint res)
         res)
       (do
         (println "No Insert Necessary; user already exists!")
         (pp/pprint i-user)
         i-user)))))

(defn get-by-id [id]
  (e/try-either
   (db/select-one m/User :id id)))

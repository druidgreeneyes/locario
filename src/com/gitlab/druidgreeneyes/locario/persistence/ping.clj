(ns com.gitlab.druidgreeneyes.locario.persistence.ping
  (:require [toucan.db :as db]
            [toucan.hydrate :as t]
            [cats.monad.either :as e]
            [cats.core :as cats]
            [com.gitlab.druidgreeneyes.locario.model :as m]
            [com.gitlab.druidgreeneyes.locario.persistence.user :as u]
            [com.gitlab.druidgreeneyes.locario.persistence.coordinate :as c]
            [clojure.pprint :as pp]))

(defn save! [ping]
  (let [user (:user ping)
        coordinate (:coordinate ping)]
    (pp/pprint ping)
    (e/try-either
     (db/transaction
      (cats/alet [r-user (u/save! user)
                  r-coord (c/save! coordinate)]
                 (->
                  {:timestamp (:timestamp ping)
                   :user_id (:id r-user)
                   :coordinate_id (:id r-coord)}
                  (->> (db/insert! m/Ping))
                  (t/hydrate :user :coordinate)))))))

(defn get-by-timestamp [timestamp]
  (e/try-either
   (db/select m/Ping :timestamp timestamp)))

(defn get-by-user-id [user-id]
  (e/try-either
   (db/select m/Ping :user_id user-id)))

(defn get-by-coordinate-id [coordinate-id]
  (e/try-either
   (db/select m/Ping :coordinate_id coordinate-id)))
